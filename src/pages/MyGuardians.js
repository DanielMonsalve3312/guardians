import React from "react";
import { Container, Content, Title, Link } from "../common/styles";
import {
  ContainerCard,
  Card,
  Info,
} from "../components/Card/styles";
import {GrAdd} from 'react-icons/gr'

const guardians = [
  {
    name: "Begrishi",
    id: 1,
    age: 3438,
    mythical: true,
  },
  {
    name: "Gammur",
    id: 2,
    age: 3478,
    owner: {
      name: "Aragon",
    },
    mythical: true,
  },
  {
    name: "Dreki",
    id: 3,
    age: 3561,
    mythical: true,
  },
  {
    name: "Griongour",
    id: 4,
    age: 3602,
    owner: true,
    mythical: true,
  },
];

export function MyGuardians() {
  return (
    <Container>
      <Content maxWidth="lg">
        <Title margin={20}>Mis guardianes</Title>
        <Link to="/create-guardian"><GrAdd/> Crear un guardian</Link>
        <ContainerCard>
          {guardians.length > 0 &&
            guardians.map((guardian) => (
              <Card key={guardian.id} cursor={true}>
                <Info>Name: {guardian.name}</Info>
                {guardian.owner && <Info>Owner: {guardian.owner.name}</Info>}
                <Info>Age: {guardian.age}</Info>
                <Info>{guardian.mythical && "Mitico"}</Info>
              </Card>
            ))}
        </ContainerCard>
      </Content>
    </Container>
  );
}
