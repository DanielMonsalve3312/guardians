import React, { useState } from "react";
import {
  Container,
  Avatar,
  Title,
  Content,
  ContainerForm,
  TextField,
  Button,
  Link,
} from "../common/styles";
import avatar from "../images/avatar.jpg";

export function SignIn() {
  const [userInfo, setInfo] = useState({
    name: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const newInfo = Object.assign({}, userInfo);
    newInfo[e.target.name] = e.target.value;

    setInfo(newInfo);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(userInfo);
  };

  return (
    <Container>
      <Content maxWidth="sm">
        <Avatar size={70} src={avatar} />
        <Title size={40}>Registrase</Title>
        <ContainerForm>
          <form onSubmit={handleSubmit}>
            <TextField
              placeholder="Nombre"
              name="name"
              onChange={handleChange}
              value={userInfo.name}
            />
            <TextField
              placeholder="Email"
              name="email"
              onChange={handleChange}
              value={userInfo.email}
            />
            <TextField
              placeholder="Password"
              type="password"
              name="password"
              onChange={handleChange}
              value={userInfo.password}
            />
            <Button color="primary" fullWidth>
              Registrase
            </Button>
          </form>
          <Link to="/login">Ya tengo una cuenta.</Link>
        </ContainerForm>
      </Content>
    </Container>
  );
}
