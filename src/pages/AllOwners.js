import React from 'react'
import { Container, Content, Title } from '../common/styles'
import { ContainerCard, Card, Info } from '../components/Card/styles'

const owners = [
    {
        name:"Aragon",
        id:1,
        age: 3438,
        guardians: [
            {name:"Begishi"},
            {name:"Dreki"},
        ]
    }
]

export function Owners() {
    return (
       <Container>
            <Content maxWidth="lg">
                <Title margin={20}>Amos</Title>
                <ContainerCard>
                    {owners.map(owner => (
                        <Card key={owner.id} cursor={true}>
                            <Info>Name: {owner.name}</Info>
                            {owner.guardians && owner.guardians.map(guardian =>(
                                <Info>Guandian: {guardian.name}</Info>
                            ))}
                            <Info>Age: {owner.age}</Info>
                        </Card>
                    ))}
                </ContainerCard>
            </Content>
       </Container>
    )
}
