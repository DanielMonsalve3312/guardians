import React from "react";
import { Container, Content, Title } from "../common/styles";
import {
  ContainerCard,
  Card,
  TitleCard,
} from "../components/Card/styles";
import miticos from "../images/mitico.jpg";
import guardians from "../images/guardian.png";
import myGuardians from "../images/myguardian.png";
import owners from "../images/owner.png";

export function Home() {
  return (
    <Container>
      <Content maxWidth="lg">
        <Title margin={25}>Busca entre las categorias</Title>
        <ContainerCard width={95}>
          <Card>
            <img src={guardians} alt="Guardian" />
            <TitleCard to="/guardians">Guardianes</TitleCard>
          </Card>
          <Card>
            <img src={miticos} alt="Mitico" />
            <TitleCard to="/mythicals">Míticos</TitleCard>
          </Card>
          <Card>
            <img src={owners} alt="Amo" />
            <TitleCard to="/owners">Amos</TitleCard>
          </Card>
          <Card>
            <img src={myGuardians} alt="Mis guardianes" />
            <TitleCard to="/my-guardians">Mis guardianes</TitleCard>
          </Card>
        </ContainerCard>
      </Content>
    </Container>
  );
}
