import React, { useState } from "react";
import {
  Container,
  Content,
  Title,
  ContainerForm,
  TextField,
  Button,
  Create,
  CreateItem,
} from "../common/styles";
import { Card, Info, ContainerCard } from "../components/Card/styles";

export function CreateGuardian() {
  const [userInfo, setInfo] = useState({
    name: "",
    age: "",
  });

  const handleChange = (e) => {
    const newInfo = Object.assign({}, userInfo);
    newInfo[e.target.name] = e.target.value;

    setInfo(newInfo);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(userInfo);
  };

  return (
    <Container>
      <Content maxWidth="md">
        <Title margin={20}>Crear guardian</Title>
        <Create>
          <CreateItem>
            <ContainerForm>
              <form onSubmit={handleSubmit}>
                <TextField
                  name="name"
                  value={userInfo.name}
                  onChange={handleChange}
                  placeholder="Nombre"
                />
                <TextField
                  name="age"
                  value={userInfo.age}
                  onChange={handleChange}
                  placeholder="Age"
                />
                <Button color="primary" fullWidth>
                  Crear
                </Button>
              </form>
            </ContainerForm>
          </CreateItem>
          <CreateItem>
            <ContainerCard width={80}>
              <Card cursor={true}>
                <Info>Name: {userInfo.name}</Info>
                <Info>Age: {userInfo.age}</Info>
              </Card>
            </ContainerCard>
          </CreateItem>
        </Create>
      </Content>
    </Container>
  );
}
