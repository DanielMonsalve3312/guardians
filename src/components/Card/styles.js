import styled from "styled-components";
import { palette } from "../../common/palette";
import { NavLink } from "react-router-dom";

export const ContainerCard = styled.div`
  width: ${(props) => props.width}%;
  padding: 10px 20px;
  display: flex;
  flex-wrap: wrap;
`;

export const Card = styled.div`
  width: 285px;
  height: 230px;
  margin: 10px 9px;
  border-radius: 10px;
  background-color: #000;
  box-shadow: 1px 1px 5px 0px ${palette.shadow};
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  cursor: ${props => props.cursor ? "pointer" : "initial"};
`;

export const TitleCard = styled(NavLink)`
  color: ${palette.bgColor};
  font-size: 25px;
  cursor: pointer;
  font-weight: 100;
`;

export const Info = styled.h2`
    color: ${palette.bgColor};
    font-weight: 100;
`