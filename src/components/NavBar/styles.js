import styled from 'styled-components'
import logo from '../../images/logo.svg'
import { palette } from '../../common/palette'
import { font } from '../../common/font'
import { NavLink } from 'react-router-dom'

export const AppNavBar = styled.div`
    background-color: ${palette.primary};
    padding: 0px 15px;
    height: 75px;
    position: fixed;
    width: 100%;
    top: 0px;
    z-index: 1000;
    box-sizing: content-box;
`

export const ToolBar = styled.div`
    display: flex;
    align-items: center;
    height: 100%;
`

export const ContainerLogo = styled.div`
    flex-grow: 1;
`
export const Logo = styled(NavLink)`
    width: 250px;
    height: 40px;
    display: inline-block;
    background-image: url(${logo});
    background-repeat: no-repeat;
    background-size: 100%;
    background-position: 50% 65%;
`

export const ContainerUser = styled.div`
    position: relative;
    width: 150px;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const Avatar = styled.div`
    width: 55px;
    height: 55px;
    border-radius: 50%;
    background-color: #fff;
    cursor: pointer;
    display: inherit;
    align-items: inherit;
    justify-content: inherit;
    & > svg {
        color: ${palette.primary};
    }
`

export const Menu = styled.button`
    width: 120px;
    height: 120px;
    border-radius: 10px;
    padding: 8px 0px;
    display: flex;
    flex-direction: column;
    box-shadow: 1px 1px 5px 0px ${palette.shadow};
    background-color: ${palette.bgColor};
    position: absolute;
    top: 70%;
    left: -45%;
    font-family: ${font.principal};
    box-sizing: content-box;
`

export const MenuItem = styled(NavLink)`
    width: 100%;
    padding: 10px 0px;
    text-align: center;
    font-size: 16px;
    cursor: pointer;
    color: ${palette.textColor};
    &:hover {
        background-color: ${palette.hover}
    }
    &[aria-current]{
        color: ${palette.primary};
    }
`
