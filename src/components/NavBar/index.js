import React, { useState, useEffect, useRef } from "react";
import {
  AppNavBar,
  ToolBar,
  ContainerLogo,
  ContainerUser,
  Logo,
  Menu,
  MenuItem,
  Avatar,
} from "./styles";
import { BsFillPersonFill } from "react-icons/bs";

export function NavBar() {
  const menuRef = useRef();
  const [menu, setMenu] = useState(false);

  useEffect(() => {
    if (menu) {
      menuRef.current.focus();
    }
  }, [menu]);

  return (
    <AppNavBar>
      <ToolBar>
        <ContainerLogo>
          <Logo to="/"/>
        </ContainerLogo>
        <ContainerUser>
          <Avatar onClick={() => setMenu(true)}>
            <BsFillPersonFill size={35} />
          </Avatar>
          {menu && (
            <Menu
              ref={menuRef}
              onBlur={() => setTimeout(() => setMenu(false), 200)}
            >
              <MenuItem to="/perfil">Perfil</MenuItem>
              <MenuItem to="/allies">Aliados</MenuItem>
              <MenuItem to="/login">Cerrar sesión</MenuItem>
            </Menu>
          )}
        </ContainerUser>
      </ToolBar>
    </AppNavBar>
  );
}
