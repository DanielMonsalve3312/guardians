import React from 'react'
import { NavBar } from './components/NavBar'
import { GlobalStyles } from './common/GlobalStyles'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Login } from './pages/Login';
import { SignIn } from './pages/SignIn';
import { Home } from './pages/Home';
import { AllGuardians } from './pages/AllGuardians';
import { Owners } from './pages/AllOwners';
import { Mythicals } from './pages/Mythicals';
import { MyGuardians } from './pages/MyGuardians';
import { CreateGuardian } from './pages/CreateGuardian';

function App () {
  return (
    <BrowserRouter>
      <GlobalStyles />
      <NavBar/>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path='/login' component={Login} />
        <Route exact path='/sign-in' component={SignIn} />
        <Route exact path='/guardians' component={AllGuardians}/>
        <Route exact path='/owners' component={Owners}/>
        <Route exact path='/mythicals' component={Mythicals}/>
        <Route exact path='/my-guardians' component={MyGuardians}/>
        <Route exact path='/create-guardian' component={CreateGuardian}/>
      </Switch>
    </BrowserRouter>
  )
}

export default App
