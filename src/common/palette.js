export const palette = {
  primary: '#1976d2',
  secondary: '',
  shadow: '#1a237e',
  common: '#e3f2fd',
  hover: '#eeeeee',
  bgColor: '#fff',
  textColor: '#616161'
}
