import styled, { css } from "styled-components";
import { palette } from "./palette";
import { font } from "./font";
import { NavLink } from "react-router-dom";

const media = {
  xs: 0,
  sm: 600,
  md: 900,
  lg: 1280,
  xl: 1920,
};

const avatar = (size, avatar) => {
  return css`
    width: ${size + 15}px;
    height: ${size + 15}px;
    background-image: url(${avatar});
  `;
};

const content = (maxWidth) => {
  return css`
    width: 100%;
    @media (min-width: ${media[maxWidth]}px) {
      min-width: ${media[maxWidth]}px;
      width: ${media[maxWidth]}px;
    }
  `;
};

export const Container = styled.div`
  padding-top: 75px;
  width: 100%;
  height: 100%;
  min-height: calc(100vh - 75px);
  display: flex;
  justify-content: center;
  font-family: ${font.principal};
`;

export const Content = styled.div`
  ${(props) => content(props.maxWidth)};
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const Avatar = styled.div`
  ${(props) => avatar(props.size, props.src)};
  border-radius: 50%;
  background-size: 100%;
  background-position: 50%;
`;

export const Title = styled.h1`
  color: ${palette.primary};
  font-size: ${(props) => (props.size ? `${props.size}px` : "auto")};
  margin: ${(props) => (props.margin ? props.margin : 15)}px 0px;
  font-weight: 100;
`;
export const ContainerForm = styled.div`
  width: 70%;
  & > form {
    width: 100%;
  }
`;

export const TextField = styled.input`
  border: 1px solid ${palette.primary};
  padding: 15px 10px;
  width: 100%;
  border-radius: 5px;
  font-size: 18px;
  margin: 8px 0px;
  outline: none;
  box-sizing: border-box;
`;

export const Button = styled.button`
  background-color: ${(props) => palette[props.color]};
  padding: 15px 15px;
  border-radius: 5px;
  width: ${(props) => (props.fullWidth ? "100%" : "auto")};
  color: ${palette.bgColor};
  font-size: 18px;
  cursor: pointer;
  transition: all 0.35s ease-out;
  margin-top: 10px;
  &:hover {
    background-color: ${palette.shadow};
  }
  &:active {
    transform: scale(0.95);
  }
`;

export const Link = styled(NavLink)`
  color: ${palette.primary};
  margin-top: 10px;
  display: inline-block;
`;

export const Create = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-evenly;
    flex-wrap: wrap;
`

export const CreateItem = styled.div`
  width: 49%;
  display: inherit;
  align-items: center;
  justify-content: center;
`