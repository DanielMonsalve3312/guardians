import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
    body{
        margin: 0px;
        padding: 0px;
    }
    a,h1,h2{
        margin:0px;
        padding: 0px;
    }
    a{
        text-decoration: none;
    }
    #root{
        scroll-behavior: smooth;
        min-height: 100vh;
        min-width: 100vw;
    }
    button{
        border: none;
        padding: 0px;
        background-color: transparent;
        outline: none;
    }
`
